import * as https from 'https';

test('Получение списка котов', async () => {
    const url = 'https://meowle.fintech-qa.ru/api/core/cats/allByLetter?limit=1';
  
    return new Promise<void>((resolve, reject) => {
      const req = https.get(url, (res) => {
        console.log('Статус код для получения списка котов:', res.statusCode);
        
        // Проверяем, что статус код равен 200
        expect(res.statusCode).toBe(200);
  
        let responseData = '';
        res.on('data', (chunk) => {
          responseData += chunk;
        });
        res.on('end', () => {
          try {
            // Парсим JSON ответ
            const jsonResponse = JSON.parse(responseData);
            // Проверяем, что ответ является объектом и содержит хотя бы одно свойство
            expect(jsonResponse).toBeInstanceOf(Object);
            expect(Object.keys(jsonResponse).length).toBeGreaterThan(0);
            // Если все проверки прошли успешно, завершаем тест
            resolve();
          } catch (error) {
            // Если произошла ошибка при парсинге JSON или проверке структуры ответа, отмечаем тест как проваленный
            reject(error);
          }
        });
      });
  
      req.on('error', (error) => {
        console.error('Произошла ошибка:', error);
        reject(error);
      });
    });
  });
  