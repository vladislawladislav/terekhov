import * as https from 'https';
import { cat_ID, makeid } from './create_cat';

// Тест на создание котика
test('создание котика', async () => {
  expect(cat_ID).toBeDefined();
});

// Тест на статус код после создания котика
//test('проверка статуса кода после создания котика', async () => {
//  expect(cat_ID).toBeDefined();
//});

// Тест на изменение описания котика

test('Изменение описания методом save_description', async () => {
    async function saveDescription(descriptionCat: string): Promise<void> {
      const url_api = 'https://meowle.fintech-qa.ru/api/core/cats/save-description';
      const catData = {
        catId: cat_ID,
        catDescription: descriptionCat
      };
      const postData = JSON.stringify(catData);
      const options = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Content-Length': Buffer.byteLength(postData)
        }
      };
  
      return new Promise<void>((resolve, reject) => {
        const req = https.request(url_api, options, (res) => {
          console.log('Статус код для сохранения описания кота:', res.statusCode);
          if (res.statusCode === 200) {
            console.log('Описание кота успешно изменено.');
            resolve();
          } else {
            console.error('Ошибка при изменении описания кота:', res.statusCode);
            reject(new Error(`Failed to save cat description. Status code: ${res.statusCode}`));
          }
        });
  
        req.on('error', (error) => {
          console.error('Произошла ошибка:', error);
          reject(error);
        });
  
        req.write(postData);
        req.end();
      });
    }
    
  
    // Генерируем случайное описание котика
    const newDescription = makeid(10);
  
    try {
      await saveDescription(newDescription);
      console.log('Новое описание котика: ' + newDescription)
    } catch (error) {
      fail(error);
    }
  });
  

// Тест на получение информации о коте по ID
test('получение информации о коте по ID', async () => {
  const url = `https://meowle.fintech-qa.ru/api/core/cats/get-by-id?id=${cat_ID}`;
  console.log(url); // Убедитесь, что URL корректно формируется

  return new Promise<void>((resolve, reject) => {
    const req = https.get(url, (res) => {
      expect(res.statusCode).toBe(200); // Проверяем, что статус код равен 200

      let responseData = '';

      res.on('data', (chunk) => {
        responseData += chunk;
      });

      res.on('end', async () => {
        const catInfo = JSON.parse(responseData);
        console.log('Информация о коте:', catInfo);
        resolve();
      });
    });

    req.on('error', (error) => {
      console.error('Произошла ошибка:', error);
      reject(error);
    });
  });
});

// Хук afterAll для выполнения удаления кота после всех остальных тестов
afterAll(async () => {
  console.log('Выполняется удаление кота...');
  
  const removeUrl = `https://meowle.fintech-qa.ru/api/core/cats/${cat_ID}/remove`;
  const removeOptions = {
    method: 'DELETE'
  };

  return new Promise<void>((resolve, reject) => {
    const removeReq = https.request(removeUrl, removeOptions, (removeRes) => {
      if (removeRes.statusCode === 200) {
        console.log('Кот успешно удален');
        resolve();
      } else {
        console.error('Ошибка при удалении кота:', removeRes.statusCode);
        reject(new Error(`Failed to remove cat. Status code: ${removeRes.statusCode}`));
      }
    });

    removeReq.on('error', (error) => {
      console.error('Произошла ошибка при удалении кота:', error);
      reject(error);
    });

    removeReq.end();
  });
});
