import * as https from 'https';

// Функция для генерации случайного некорректного id
function generateRandomCharacters(): string {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()';
    let result = '';
    for (let i = 0; i < 5; i++) {
      result += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return result;
}

// Функция для отправки GET-запроса по ID
function getCatInfoByID(catID: string): Promise<number> {
  const url = `https://meowle.fintech-qa.ru/api/core/cats/get-by-id?id=${catID}`;

  return new Promise<number>((resolve, reject) => {
    const req = https.get(url, (res) => {
      console.log(`Статус код для кота с ID ${catID}:`, res.statusCode);
      resolve(res.statusCode); // Возвращаем статус код ответа
    });

    req.on('error', (error) => {
      console.error('Произошла ошибка:', error);
      reject(error);
    });
  });
}

// Генерируем случайные некорректные ID
const incorrectIDs: string[] = [];
for (let i = 0; i < 5; i++) {
  incorrectIDs.push(generateRandomCharacters());
}

// Тест на получение информации о коте по некорректному ID и проверку статуса кода ответа сервера
test('получение информации о коте по некорректному ID и проверка статуса кода ответа', async () => {
    const statuses = await Promise.all(incorrectIDs.map(id => getCatInfoByID(id)));
    statuses.forEach((status, index) => {
      // Проверяем, что статус код равен 400 для каждого запроса
      expect(status).toBe(400); 
    });
}, 5000); // Увеличиваем таймаут до 5 секунд
