import * as https from 'https';

let cat_ID: number;
// Функция для создания котика
async function createCat(nameCat: string): Promise<number> {
  const url_api = 'https://meowle.fintech-qa.ru/api/core/cats/add';
  const catData = {
    cats: [
      {
        name: nameCat,
        description: 'Описание кота',
        gender: 'unisex'
      }
    ]
  };
  const postData = JSON.stringify(catData);
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Content-Length': Buffer.byteLength(postData)
    }
  };
  
  return new Promise<number>((resolve, reject) => {
    const req = https.request(url_api, options, (res) => {
      expect(res.statusCode).toBe(200); // Проверяем, что статус код равен 200 при успешном создании кота

      let responseData = '';
      res.on('data', (chunk) => {
        responseData += chunk;
      });
      res.on('end', () => {
        const responseObj = JSON.parse(responseData);
        const cat_ID = responseObj.cats[0].id;
        console.log('ID кота:', cat_ID);
        resolve(cat_ID);
      });
    });
    
    req.on('error', (error) => {
      console.error('Произошла ошибка:', error);
      reject(error);
    });
    
    req.write(postData);
    req.end();
  });
}

// Генерируем случайное имя котика
function makeid(length) {
  let result = '';
  const characters = 'АБВГДЕЁЖЗИКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ';
  const charactersLength = characters.length;
  let counter = 0;
  while (counter < length) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
    counter += 1;
  }
  return result;
}

// Начало тестов
beforeAll(async () => {
  // Выполняется до начала всех тестов
  const randomCatName = makeid(5); // Генерируем случайное имя кота
  cat_ID = await createCat(randomCatName); // Создаем кота и получаем его ID
});

// Пример теста
test('Создание кота', () => {
  expect(cat_ID).toBeDefined(); // Проверяем, что ID кота определен
});
export { cat_ID, makeid };